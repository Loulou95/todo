<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        factory(App\User::class, 1)->create([
            'id' => $faker->randomNumber(9),
            'name' => 'Egbe',
            'email' => 'egbe@yahoo.com',
            'password' => Hash::make('password12')
        ]);
    }
}
